import { AbstractControl, ValidationErrors } from '@angular/forms';

export function matchValue(matchTo: string): (abstractControl: AbstractControl) => ValidationErrors | null {
  return (c: AbstractControl): ValidationErrors | null => {
    const error = { isMatching: false };

    return !!c.parent && !!c.parent.value && c.value === c.parent.controls[matchTo].value ? null : error;
  };
}
