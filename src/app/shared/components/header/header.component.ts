import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

import * as fromAuth from '../../../auth/reducers';
import { Logout, ChangeLanguage } from '../../../auth/actions/auth.actions';
import { ROUTES } from '../../../app.constants';
import { CurrentLanguage } from '../../../auth/models/current-language';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  ROUTES = ROUTES;

  isLoggedIn$: Observable<boolean>;

  constructor(
    private router: Router,
    private authStore: Store<fromAuth.AuthState>,
    private translate: TranslateService,
  ) {
    this.isLoggedIn$ = this.authStore.pipe(select(fromAuth.getIsLoggedIn));
  }

  logout(): void {
    this.authStore.dispatch(new Logout());
  }

  toggleLanguage(): void {
    if (this.translate.getDefaultLang() === CurrentLanguage.ENGLISH) {
      this.authStore.dispatch(new ChangeLanguage(CurrentLanguage.ESTONIAN));
    } else {
      this.authStore.dispatch(new ChangeLanguage(CurrentLanguage.ENGLISH));
    }
  }

  getCurrentRoute(): string {
    return this.router.url;
  }
}
