import { Action } from '@ngrx/store';

import { Credentials } from '../models/credentials';
import { CurrentLanguage } from '../models/current-language';
import { NewUser } from '../models/new-user';
import { LoginResponse } from '../models/login-response';
import { Email } from '../../user/models/email';
import { ResetPasswordParams } from '../models/reset-password-params';

export enum AuthActionTypes {
  Login                       = '[Auth] Login',
  LoginSuccess                = '[Auth] Login success',
  LoginFailure                = '[Auth] Login failure',
  Logout                      = '[Auth] Logout',
  LoginRedirect               = '[Auth] Login redirect',
  ChangeLanguage              = '[Auth] Change language',
  RegisterUser                = '[Auth] Register user',
  RegisterUserSuccess         = '[Auth] Register user success',
  RegisterUserFailure         = '[Auth] Register user failure',
  ActivateUser                = '[Auth] Activate user',
  ActivateUserSuccess         = '[Auth] Activate user success',
  ActivateUserFailure         = '[Auth] Activate user failure',
  RequestPasswordReset        = '[Auth] Request password reset',
  RequestPasswordResetSuccess = '[Auth] Request password reset success',
  RequestPasswordResetFailure = '[Auth] Request password reset failure',
  ResetPassword               = '[Auth] Reset password',
  ResetPasswordSuccess        = '[Auth] Reset password success',
  ResetPasswordFailure        = '[Auth] Reset password failure',
  Forbidden                   = '[Auth] Forbidden',
}

export class Login implements Action {
  readonly type = AuthActionTypes.Login;

  constructor(public payload: Credentials) { }
}

export class LoginSuccess implements Action {
  readonly type = AuthActionTypes.LoginSuccess;

  constructor(public payload: LoginResponse) { }
}

export class LoginFailure implements Action {
  readonly type = AuthActionTypes.LoginFailure;

  constructor(public payload: any) { }
}

export class Logout implements Action {
  readonly type = AuthActionTypes.Logout;
}

export class LoginRedirect implements Action {
  readonly type = AuthActionTypes.LoginRedirect;
}

export class ChangeLanguage implements Action {
  readonly type = AuthActionTypes.ChangeLanguage;

  constructor(public payload: CurrentLanguage) { }
}

export class RegisterUser implements Action {
  readonly type = AuthActionTypes.RegisterUser;

  constructor(public payload: NewUser) { }
}

export class RegisterUserSuccess implements Action {
  readonly type = AuthActionTypes.RegisterUserSuccess;
}

export class RegisterUserFailure implements Action {
  readonly type = AuthActionTypes.RegisterUserFailure;

  constructor(public payload: any) { }
}

export class ActivateUser implements Action {
  readonly type = AuthActionTypes.ActivateUser;

  constructor(public payload: string) { }
}

export class ActivateUserSuccess implements Action {
  readonly type = AuthActionTypes.ActivateUserSuccess;
}

export class ActivateUserFailure implements Action {
  readonly type = AuthActionTypes.ActivateUserFailure;

  constructor(public payload: any) { }
}

export class RequestPasswordReset implements Action {
  readonly type = AuthActionTypes.RequestPasswordReset;

  constructor(public payload: Email) { }
}

export class RequestPasswordResetSuccess implements Action {
  readonly type = AuthActionTypes.RequestPasswordResetSuccess;
}

export class RequestPasswordResetFailure implements Action {
  readonly type = AuthActionTypes.RequestPasswordResetFailure;

  constructor(public payload: any) { }
}

export class ResetPassword implements Action {
  readonly type = AuthActionTypes.ResetPassword;

  constructor(public payload: ResetPasswordParams) { }
}

export class ResetPasswordSuccess implements Action {
  readonly type = AuthActionTypes.ResetPasswordSuccess;
}

export class ResetPasswordFailure implements Action {
  readonly type = AuthActionTypes.ResetPasswordFailure;

  constructor(public payload: any) { }
}

export class Forbidden implements Action {
  readonly type = AuthActionTypes.Forbidden;
}

export type AuthActions =
  | Login
  | LoginSuccess
  | LoginFailure
  | Logout
  | LoginRedirect
  | ChangeLanguage
  | RegisterUser
  | RegisterUserSuccess
  | RegisterUserFailure
  | ActivateUser
  | ActivateUserSuccess
  | ActivateUserFailure
  | RequestPasswordReset
  | RequestPasswordResetSuccess
  | RequestPasswordResetFailure
  | ResetPassword
  | ResetPasswordSuccess
  | ResetPasswordFailure
  | Forbidden;
