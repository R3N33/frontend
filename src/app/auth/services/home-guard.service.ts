import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { take, map } from 'rxjs/operators';

import * as fromAuth from '../reducers';

@Injectable()
export class HomeGuard implements CanActivate {

  constructor(private authStore: Store<fromAuth.AuthState>) { }

  canActivate(): Observable<boolean> {
    return this.authStore.pipe(
      select(fromAuth.getIsLoggedIn),
      map((isLoggedIn) => !isLoggedIn),
      take(1),
    );
  }
}
