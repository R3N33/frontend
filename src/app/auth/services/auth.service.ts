import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Credentials } from '../models/credentials';
import { LoginResponse } from '../models/login-response';
import { NewUser } from '../models/new-user';
import { API_ENDPOINTS } from '../../app.constants';
import { Email } from '../../user/models/email';
import { ResetPasswordParams } from '../models/reset-password-params';


@Injectable()
export class AuthService {
  private authUrl          = `API_URL/${API_ENDPOINTS.auth}`;
  private activationUrl    = `API_URL/${API_ENDPOINTS.activateUser}`;
  private registrationUrl  = `API_URL/${API_ENDPOINTS.registerUser}`;
  private resetPasswordUrl = `API_URL/${API_ENDPOINTS.resetPassword}`;

  constructor(private httpClient: HttpClient) { }

  login$(credentials: Credentials): Observable<LoginResponse> {
    return this.httpClient.post<LoginResponse>(this.authUrl, credentials);
  }

  register$(newUser: NewUser): Observable<any> {
    return this.httpClient.post<any>(this.registrationUrl, newUser);
  }

  activate$(activationKey: string): Observable<any> {
    return this.httpClient.get<any>(`${this.activationUrl}/${activationKey}`);
  }

  requestPasswordReset$(email: Email): Observable<any> {
    return this.httpClient.post<any>(this.resetPasswordUrl, email);
  }

  resetPassword$(params: ResetPasswordParams): Observable<any> {
    return this.httpClient.post<any>(`${this.resetPasswordUrl}/${params.token}`, params.credentials);
  }
}
