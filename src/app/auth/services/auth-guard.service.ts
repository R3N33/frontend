import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';

import * as fromAuth from '../reducers';
import { LoginRedirect } from '../actions/auth.actions';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authStore: Store<fromAuth.AuthState>) { }

  canActivate(): Observable<boolean> {
    return this.authStore.pipe(
      select(fromAuth.getIsLoggedIn),
      map((isLoggedIn) => {
        if (!isLoggedIn) {
          this.authStore.dispatch(new LoginRedirect());
        }

        return isLoggedIn;
      }),
      take(1),
    );
  }
}
