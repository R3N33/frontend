import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { take, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import * as fromAuth from '../../reducers';
import { ActivateUser, AuthActionTypes } from '../../actions/auth.actions';
import { ROUTES } from '../../../app.constants';

@Component({
  selector: 'app-activation-page',
  templateUrl: './activation-page.component.html',
  styleUrls: ['./activation-page.component.scss']
})
export class ActivationPageComponent implements OnInit {
  isPending$: Observable<boolean>;
  isSuccess$: Observable<boolean>;
  activationKey: string;

  ROUTES = ROUTES;

  constructor(
    private actions: Actions,
    private authStore: Store<fromAuth.AuthState>,
    private activatedRoute: ActivatedRoute,
  ) {
    this.isPending$    = this.authStore.pipe(select(fromAuth.getPending));
    this.activationKey = this.activatedRoute.snapshot.paramMap.get('activationKey');
  }

  ngOnInit(): void {
    this.authStore.dispatch(new ActivateUser(this.activationKey));

    this.isSuccess$ = this.actions.pipe(
      ofType(AuthActionTypes.ActivateUserSuccess),
      take(1),
      map(() => true),
    );
  }
}
