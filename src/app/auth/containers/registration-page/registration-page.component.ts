import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { ofType, Actions } from '@ngrx/effects';

import { ROUTES, PASSWORD_MIN_LENGTH } from '../../../app.constants';
import * as fromAuth from '../../reducers';
import { NewUser } from '../../models/new-user';
import { RegisterUser, AuthActionTypes } from '../../actions/auth.actions';
import { matchValue } from '../../../shared/helpers/custom-validators';

@Component({
  selector: 'app-registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.scss']
})
export class RegistrationPageComponent {
  isPending$: Observable<boolean>;
  isSuccess$: Observable<boolean>;
  registrationForm: FormGroup;

  ROUTES                          = ROUTES;
  isRedirecting                   = false;
  PASSWORD_MIN_LENGTH             = PASSWORD_MIN_LENGTH;

  constructor(
    private actions: Actions,
    private authStore: Store<fromAuth.AuthState>,
    private formBuilder: FormBuilder,
  ) {
    this.isPending$       = this.authStore.pipe(select(fromAuth.getPending));
    this.registrationForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      confirmEmail: ['', [Validators.required, matchValue('email')]],
      name: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(PASSWORD_MIN_LENGTH)]],
      confirmPassword: ['', [Validators.required, matchValue('password')]],
    }, { updateOn: 'submit' });
  }

  onSubmit(): void {
    if (this.registrationForm.invalid) {
      return;
    }

    this.authStore.dispatch(new RegisterUser(this.registrationForm.value as NewUser));

    this.isSuccess$ = this.actions.pipe(
      ofType(AuthActionTypes.RegisterUserSuccess),
      take(1),
      map(() => true),
    );
  }
}
