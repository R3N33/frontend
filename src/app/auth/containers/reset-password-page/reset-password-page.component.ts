import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { ofType, Actions } from '@ngrx/effects';
import { ActivatedRoute } from '@angular/router';
import { take, map } from 'rxjs/operators';

import * as fromAuth from '../../reducers';
import { PASSWORD_MIN_LENGTH } from '../../../app.constants';
import { RequestPasswordReset, AuthActionTypes, ResetPassword } from '../../actions/auth.actions';
import { Email } from '../../../user/models/email';
import { ResetPasswordParams } from '../../models/reset-password-params';
import { matchValue } from '../../../shared/helpers/custom-validators';

@Component({
  selector: 'app-reset-password-page',
  templateUrl: './reset-password-page.component.html',
  styleUrls: ['./reset-password-page.component.scss']
})
export class ResetPasswordPageComponent {
  token: string;
  isSuccess$: Observable<boolean>;
  isPending$: Observable<boolean>;
  requestResetForm: FormGroup;
  resetPasswordForm: FormGroup;

  PASSWORD_MIN_LENGTH = PASSWORD_MIN_LENGTH;

  constructor(
    private actions: Actions,
    private authStore: Store<fromAuth.AuthState>,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
  ) {
    this.token            = this.activatedRoute.snapshot.paramMap.get('token');
    this.isPending$       = this.authStore.pipe(select(fromAuth.getPending));
    this.requestResetForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      confirmEmail: ['', [Validators.required, matchValue('email')]],
    }, { updateOn: 'submit' });

    this.resetPasswordForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      confirmEmail: ['', [Validators.required, matchValue('email')]],
      password: ['', [Validators.required, Validators.minLength(PASSWORD_MIN_LENGTH)]],
      confirmPassword: ['', [Validators.required, matchValue('password')]],
    }, { updateOn: 'submit' });
  }

  onRequestPasswordResetSubmit(): void {
    if (this.requestResetForm.invalid) {
      return;
    }

    this.authStore.dispatch(new RequestPasswordReset({ email: this.requestResetForm.value.email } as Email));

    this.isSuccess$ = this.actions.pipe(
      ofType(AuthActionTypes.RequestPasswordResetSuccess),
      take(1),
      map(() => true),
    );
  }

  onResetPasswordSubmit(): void {
    if (this.resetPasswordForm.invalid) {
      return;
    }

    const resetPasswordParams = {
      credentials: {
        email: this.resetPasswordForm.value.email,
        password: this.resetPasswordForm.value.password
      },
      token: this.token,
     } as ResetPasswordParams;

    this.authStore.dispatch(new ResetPassword(resetPasswordParams));

    this.isSuccess$ = this.actions.pipe(
      ofType(AuthActionTypes.ResetPasswordSuccess),
      take(1),
      map(() => true),
    );
  }
}
