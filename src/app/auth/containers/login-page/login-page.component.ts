import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import * as fromAuth from '../../reducers';
import { Login } from '../../actions/auth.actions';
import { Credentials } from '../../models/credentials';
import { ROUTES } from '../../../app.constants';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent {
  loginForm: FormGroup;

  ROUTES = ROUTES;

  constructor(
    private authStore: Store<fromAuth.AuthState>,
    private formBuilder: FormBuilder,
  ) {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    }, { updateOn: 'submit' });
  }

  onSubmit(): void {
    if (this.loginForm.invalid) {
      return;
    }

    this.authStore.dispatch(new Login(this.loginForm.value as Credentials));
  }
}
