export enum CurrentLanguage {
  ENGLISH  = 'en-EN',
  ESTONIAN = 'et-ET',
}
