export interface UserInfo {
  id: number;
  name: string;
  email: string;
  isActivated: boolean;
}
