import { Credentials } from './credentials';

export interface ResetPasswordParams {
  credentials: Credentials;
  token: string;
}
