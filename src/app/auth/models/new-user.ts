export interface NewUser {
  email: string;
  confirmEmail: string;
  name: string;
  password: string;
  confirmPassword: string;
}
