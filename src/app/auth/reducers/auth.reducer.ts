import { JwtHelperService } from '@auth0/angular-jwt';

import { AuthActions, AuthActionTypes } from '../actions/auth.actions';
import { CurrentLanguage } from '../models/current-language';
import { UserInfo } from '../models/user-info';
import { UserActionTypes, UserActions } from '../../user/actions/user.actions';


export interface AuthState {
  isLoggedIn: boolean;
  accessToken: string;
  currentLang: CurrentLanguage;
  userInfo: UserInfo;
  pending: boolean;
  error: any;
}

export const initialState: AuthState = {
  isLoggedIn: false,
  accessToken: null,
  currentLang: CurrentLanguage.ENGLISH,
  userInfo: null,
  pending: false,
  error: null,
};

export function reducer(state = initialState, action: AuthActions | UserActions): AuthState {
  switch (action.type) {
    case AuthActionTypes.Login: {
      return {
        ...initialState,
        currentLang: state.currentLang,
        pending: true,
      };
    }

    case AuthActionTypes.LoginSuccess: {
      const jwtHelper = new JwtHelperService();
      const userInfo  = jwtHelper.decodeToken(action.payload.accessToken);

      return {
        ...state,
        isLoggedIn: true,
        accessToken: action.payload.accessToken,
        userInfo: {
          id: userInfo.id,
          name: userInfo.name,
          email: userInfo.email,
          isActivated: userInfo.isActivated,
        },
        pending: false,
      };
    }

    case AuthActionTypes.LoginFailure: {
      return {
        ...state,
        error: action.payload,

      };
    }

    case UserActionTypes.EditUserSuccess: {
      return {
        ...state,
        userInfo: {
          id: action.payload.id,
          name: action.payload.name,
          email: action.payload.email,
          isActivated: action.payload.isActivated ? true : false,
        },
        pending: false,
      };
    }

    case AuthActionTypes.Logout: {
      return {
        ...initialState,
        currentLang: state.currentLang,
      };
    }

    case AuthActionTypes.ChangeLanguage: {
      return {
        ...state,
        currentLang: action.payload,
      };
    }

    case AuthActionTypes.RegisterUser: {
      return {
        ...state,
        pending: true,
      };
    }

    case AuthActionTypes.RegisterUserSuccess: {
      return {
        ...state,
        pending: false,
      };
    }

    case AuthActionTypes.RegisterUserFailure: {
      return {
        ...initialState,
        currentLang: state.currentLang,
        pending: false,
        error: action.payload,
      };
    }

    case AuthActionTypes.ActivateUser: {
      return {
        ...state,
        pending: true,
      };
    }

    case AuthActionTypes.ActivateUserSuccess: {
      return {
        ...state,
        pending: false,
      };
    }

    case AuthActionTypes.ActivateUserFailure: {
      return {
        ...state,
        pending: false,
        error: action.payload,
      };
    }

    case AuthActionTypes.RequestPasswordReset: {
      return {
        ...state,
        pending: true,
      };
    }

    case AuthActionTypes.RequestPasswordResetSuccess: {
      return {
        ...state,
        pending: false,
      };
    }

    case AuthActionTypes.RequestPasswordResetFailure: {
      return {
        ...state,
        pending: false,
        error: action.payload,
      };
    }

    case AuthActionTypes.ResetPassword: {
      return {
        ...state,
        pending: true,
      };
    }

    case AuthActionTypes.ResetPasswordSuccess: {
      return {
        ...state,
        pending: false,
      };
    }

    case AuthActionTypes.ResetPasswordFailure: {
      return {
        ...state,
        pending: false,
        error: action.payload,
      };
    }

    case AuthActionTypes.Forbidden: {
      return {
        ...initialState,
      };
    }

    case AuthActionTypes.LoginRedirect: {
      return {
        ...initialState,
        currentLang: state.currentLang,
      };
    }

    default: {
      return state;
    }
  }
}

export const getError       = (state: AuthState) => state.error;
export const getPending     = (state: AuthState) => state.pending;
export const getUserInfo    = (state: AuthState) => state.userInfo;
export const getIsLoggedIn  = (state: AuthState) => state.isLoggedIn;
export const getAccessToken = (state: AuthState) => state.accessToken;
export const getCurrentLang = (state: AuthState) => state.currentLang;
