
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromAuth from './auth.reducer';

export interface AuthState {
  auth: fromAuth.AuthState;
}

export const reducers: ActionReducerMap<AuthState> = {
  auth: fromAuth.reducer,
};

export const selectAuthState = createSelector(
  createFeatureSelector<AuthState>('auth'),
  (state: AuthState) => state.auth,
);

export const getIsLoggedIn = createSelector(
  selectAuthState,
  fromAuth.getIsLoggedIn,
);

export const getAccessToken = createSelector(
  selectAuthState,
  fromAuth.getAccessToken,
);

export const getCurrentLang = createSelector(
  selectAuthState,
  fromAuth.getCurrentLang,
);

export const getPending = createSelector(
  selectAuthState,
  fromAuth.getPending,
);

export const getError = createSelector(
  selectAuthState,
  fromAuth.getError,
);

export const getUserInfo = createSelector(
  selectAuthState,
  fromAuth.getUserInfo,
);
