import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { localStorageSync } from 'ngrx-store-localstorage';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule, MetaReducer, ActionReducer } from '@ngrx/store';

import { LoginPageComponent } from './containers/login-page/login-page.component';
import { AuthRoutingModule } from './auth-routing.module';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth-guard.service';
import { HomeGuard } from './services/home-guard.service';
import { AuthEffects } from './effects/auth.effects';
import { reducers } from './reducers';
import { SharedModule } from '../shared/shared.module';
import { ResetPasswordPageComponent } from './containers/reset-password-page/reset-password-page.component';
import { RegistrationPageComponent } from './containers/registration-page/registration-page.component';
import { ActivationPageComponent } from './containers/activation-page/activation-page.component';

export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync({
    keys: ['auth'],
    rehydrate: true,
  })(reducer);
}

const metaReducers: MetaReducer<any, any>[] = [localStorageSyncReducer];

@NgModule({
  declarations: [
    LoginPageComponent,
    ResetPasswordPageComponent,
    RegistrationPageComponent,
    ActivationPageComponent,
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    StoreModule.forFeature('auth', reducers, { metaReducers }),
    EffectsModule.forFeature([AuthEffects]),
    SharedModule,
  ],
  exports: [],
  providers: [
    AuthService,
    AuthGuard,
    HomeGuard,
  ],
})
export class AuthModule { }
