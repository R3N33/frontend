import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeGuard } from './services/home-guard.service';
import { LoginPageComponent } from './containers/login-page/login-page.component';
import { ResetPasswordPageComponent } from './containers/reset-password-page/reset-password-page.component';
import { ROUTES } from '../app.constants';
import { RegistrationPageComponent } from './containers/registration-page/registration-page.component';
import { ActivationPageComponent } from './containers/activation-page/activation-page.component';

const routes: Routes = [
  {
    path: ROUTES.login,
    component: LoginPageComponent,
    canActivate: [HomeGuard],
  },
  {
    path: ROUTES.registration,
    component: RegistrationPageComponent,
    canActivate: [HomeGuard],
  },
  {
    path: ROUTES.resetPassword,
    component: ResetPasswordPageComponent,
    canActivate: [HomeGuard],
  },
  {
    path: `${ROUTES.resetPassword}/:token`,
    component: ResetPasswordPageComponent,
    canActivate: [HomeGuard],
  },
  {
    path: `${ROUTES.activateUser}/:activationKey`,
    component: ActivationPageComponent,
    canActivate: [HomeGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule { }
