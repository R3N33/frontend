import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, exhaustMap, catchError } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { of } from 'rxjs';

import { AuthService } from '../services/auth.service';
import {
  AuthActionTypes,
  Login, LoginSuccess,
  LoginFailure,
  ChangeLanguage,
  RegisterUser,
  RegisterUserSuccess,
  RegisterUserFailure,
  ActivateUser,
  ActivateUserSuccess,
  ActivateUserFailure,
  RequestPasswordReset,
  RequestPasswordResetSuccess,
  RequestPasswordResetFailure,
  ResetPassword,
  ResetPasswordSuccess,
  ResetPasswordFailure,
} from '../actions/auth.actions';
import { Credentials } from '../models/credentials';
import { ROUTES } from '../../app.constants';
import { LoginResponse } from '../models/login-response';
import { NewUser } from '../models/new-user';
import { Email } from '../../user/models/email';
import { ResetPasswordParams } from '../models/reset-password-params';

@Injectable()
export class AuthEffects {
  constructor(
    private router: Router,
    private actions$: Actions,
    private translate: TranslateService,
    private authService: AuthService,
  ) { }

  @Effect()
  login$ = this.actions$.pipe(
    ofType(AuthActionTypes.Login),
    map((action: Login) => action.payload),
    exhaustMap((credentials: Credentials) =>
      this.authService.login$(credentials).pipe(
        map((response: LoginResponse) => {
          this.router.navigate([`/${ROUTES.users}`]);

          return new LoginSuccess(response);
        }),
        catchError((error: any) => of(new LoginFailure(error))),
      ),
    ),
  );

  @Effect()
  register$ = this.actions$.pipe(
    ofType(AuthActionTypes.RegisterUser),
    map((action: RegisterUser) => action.payload),
    exhaustMap((newUser: NewUser) =>
      this.authService.register$(newUser).pipe(
        map(() => new RegisterUserSuccess()),
        catchError((error: any) => of(new RegisterUserFailure(error))),
      ),
    ),
  );

  @Effect()
  activate$ = this.actions$.pipe(
    ofType(AuthActionTypes.ActivateUser),
    map((action: ActivateUser) => action.payload),
    exhaustMap((activationKey: string) =>
      this.authService.activate$(activationKey).pipe(
        map(() => new ActivateUserSuccess()),
        catchError((error: any) => of(new ActivateUserFailure(error))),
      ),
    ),
  );

  @Effect()
  requestPasswordReset$ = this.actions$.pipe(
    ofType(AuthActionTypes.RequestPasswordReset),
    map((action: RequestPasswordReset) => action.payload),
    exhaustMap((email: Email) =>
      this.authService.requestPasswordReset$(email).pipe(
        map(() => new RequestPasswordResetSuccess()),
        catchError((error: any) => of(new RequestPasswordResetFailure(error))),
      ),
    ),
  );

  @Effect()
  resetPassword$ = this.actions$.pipe(
    ofType(AuthActionTypes.ResetPassword),
    map((action: ResetPassword) => action.payload),
    exhaustMap((params: ResetPasswordParams) =>
      this.authService.resetPassword$(params).pipe(
        map(() => new ResetPasswordSuccess()),
        catchError((error: any) => of(new ResetPasswordFailure(error))),
      ),
    ),
  );

  @Effect({ dispatch: false })
  loginRedirect$ = this.actions$.pipe(
    ofType(AuthActionTypes.LoginRedirect, AuthActionTypes.Forbidden),
    exhaustMap(() => this.router.navigate([`/${ROUTES.login}`])),
  );

  @Effect({ dispatch: false })
  logout$ = this.actions$.pipe(
    ofType(AuthActionTypes.Logout),
    exhaustMap(() => this.router.navigate([`/${ROUTES.login}`])),
  );

  @Effect({ dispatch: false })
  changeLanguage$ = this.actions$.pipe(
    ofType(AuthActionTypes.ChangeLanguage),
    map((action: ChangeLanguage) => action.payload),
    exhaustMap((lang) => {
      this.translate.setDefaultLang(lang);

      return of(true);
    }),
  );
}
