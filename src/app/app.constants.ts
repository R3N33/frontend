export const API_URL = 'http://localhost:3600';

export const ROUTES = {
  users: 'users',
  login: 'login',
  auth: 'auth',
  resetPassword: 'reset-password',
  registration: 'registration',
  usersInfo: 'users/info',
  activateUser: 'users/activate',
};

export const PROTECTED_ROUTES = [
  ROUTES.users,
  ROUTES.usersInfo,
];

export const PUBLIC_ROUTES = [
  ROUTES.login,
  ROUTES.auth,
  ROUTES.resetPassword,
  ROUTES.registration,
  ROUTES.activateUser,
];

export const API_ENDPOINTS = {
  users: 'users',
  auth: 'auth',
  activateUser: 'users/activate',
  deleteUser: 'users/delete',
  addNewUser: 'users/new',
  registerUser: 'users/register',
  userLoginHistory: 'users/history',
  resetPassword: 'users/reset-password',
  editUser: 'users/edit',
};

export const PASSWORD_MIN_LENGTH = 8;

export const PAGE_SIZE_OPTIONS = [5, 10, 25, 100];

export const INITIAL_PAGE_SIZE = 10;

export const LOGIN_HISTORY_DIALOG_ID = 'loginHistoryDialog';
