import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotFoundPageComponent } from './core/not-found-page/not-found-page.component';
import { ROUTES } from './app.constants';


const routes: Routes = [
  {
    path: '',
    redirectTo: `/${ROUTES.users}`,
    pathMatch: 'full',
  },
  {
    path: '**',
    component: NotFoundPageComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule { }
