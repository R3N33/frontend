import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, combineLatest } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { take, mergeMap, tap } from 'rxjs/operators';

import { API_URL, PUBLIC_ROUTES } from '../../../app/app.constants';
import * as fromAuth from '../../auth/reducers';
import { ErrorsHandler } from './errors-handler.service';

@Injectable()
export class RequestInterceptorService implements HttpInterceptor {
  constructor(
    private authStore: Store<fromAuth.AuthState>,
    private errorsHandler: ErrorsHandler,
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return combineLatest([
      this.authStore.pipe(select(fromAuth.getAccessToken)),
      this.authStore.pipe(select(fromAuth.getCurrentLang)),
    ]).pipe(
      take(1),
      mergeMap(([accessToken, currentLang]) => {
        const urlParts = request.url.split('/');
        const isPublic = urlParts.some((urlPart) => PUBLIC_ROUTES.includes(urlPart));

        const modifiedRequest = request.clone({
          url: request.url.replace('API_URL', API_URL),
          ...(!isPublic && { setHeaders: { Authorization: `Bearer ${accessToken}`, 'Content-Language': currentLang } }),
          ...(isPublic && { setHeaders: { 'Content-Language': currentLang } })
        });

        return next.handle(modifiedRequest).pipe(
          tap((event: HttpEvent<any>) => {}, (err: any) => this.errorsHandler.handleError(err)),
        );
      }),
    );
  }
}
