import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { HttpErrorResponse } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';

import { HttpStatusCode } from '../../shared/models/http-status-code';
import * as fromAuth from '../../auth/reducers';
import { Forbidden } from '../../auth/actions/auth.actions';

@Injectable()
export class ErrorsHandler implements ErrorsHandler {
  private clientErrorCodes  = [ HttpStatusCode.UNAUTHORIZED, HttpStatusCode.FORBIDDEN ];
  private genericErrorCodes = [
    HttpStatusCode.INTERNAL_SERVER_ERROR,
    HttpStatusCode.NOT_IMPLEMENTED,
    HttpStatusCode.BAD_GATEWAY,
    HttpStatusCode.SERVICE_UNAVAILABLE,
    HttpStatusCode.GATEWAY_TIMEOUT,
    HttpStatusCode.HTTP_VERSION_NOT_SUPPORTED,
    HttpStatusCode.VARIANT_ALSO_NEGOTIATES,
    HttpStatusCode.INSUFFICIENT_STORAGE,
    HttpStatusCode.LOOP_DETECTED,
    HttpStatusCode.NOT_EXTENDED,
    HttpStatusCode.NETWORK_AUTHENTICATION_REQUIRED,
    HttpStatusCode.UNPROCESSABLE_ENTITY,
    HttpStatusCode.CONFLICT,
    HttpStatusCode.NOT_FOUND,
    HttpStatusCode.BAD_REQUEST,
  ];

  constructor(
    private snackBar: MatSnackBar,
    private authStore: Store<fromAuth.AuthState>,
    private translateService: TranslateService,
  ) { }

  handleError(errorResponse: HttpErrorResponse | any): void {
    if (errorResponse instanceof HttpErrorResponse) {
      const hasCustomErrors = !!errorResponse.error && !!errorResponse.error.errors;

      if (this.clientErrorCodes.includes(errorResponse.status)) {
        this.translateService.get('errors.sessionExpired').subscribe((translation) => this.showError(translation));
        this.authStore.dispatch(new Forbidden());
      } else if (this.genericErrorCodes.includes(errorResponse.status) && hasCustomErrors) {
        const errors = errorResponse.error.errors;

        errors.length > 1 ? this.showError(errors.join('\n')) : this.showError(errors.toString());
      } else if (this.genericErrorCodes.includes(errorResponse.status)) {
        this.translateService.get('errors.httpErrorOccured').subscribe((translation) =>
          this.showError(`${translation}: ${errorResponse.status} ${errorResponse.statusText}`),
        );
      }
    } else {
      console.error(errorResponse);
    }
  }

  private showError(errorMessage: string): void {
    this.snackBar.open(errorMessage, 'OK', {
      duration: 3000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    });
  }
}
