import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Location } from '@angular/common';

import * as fromAuth from '../../auth/reducers';
import { ROUTES } from '../../app.constants';

@Component({
  selector: 'app-not-found-page',
  templateUrl: './not-found-page.component.html',
  styleUrls: ['./not-found-page.component.scss']
})
export class NotFoundPageComponent {
  isLoggedIn$: Observable<boolean>;

  constructor(
    private router: Router,
    private location: Location,
    private authStore: Store<fromAuth.AuthState>,
  ) {
    this.isLoggedIn$ = this.authStore.pipe(select(fromAuth.getIsLoggedIn));
  }

  goBack(): void {
    this.location.back();
  }

  goHome(): void {
    this.router.navigate([`/${ROUTES.users}`]);
  }
}
