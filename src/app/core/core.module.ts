import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NotFoundPageComponent } from './not-found-page/not-found-page.component';
import { ErrorsHandler } from './services/errors-handler.service';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    NotFoundPageComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
  ],
  exports: [
    NotFoundPageComponent,
  ],
  providers: [
    ErrorsHandler,
  ],
})
export class CoreModule { }
