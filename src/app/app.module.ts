import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { StoreModule, Store, select } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { take } from 'rxjs/operators';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from 'src/environments/environment';
import { RequestInterceptorService } from './core/services/request-interceptor.service';
import { ErrorsHandler } from './core/services/errors-handler.service';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { AuthGuard } from './auth/services/auth-guard.service';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import * as fromAuth from './auth/reducers';
import { ConfirmationDialogComponent } from './shared/components/confirmation-dialog/confirmation-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    StoreModule.forRoot({}, {}),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
    EffectsModule.forRoot([]),
    SharedModule,
    CoreModule,
    AuthModule,
    UserModule,
    AppRoutingModule,
  ],
  providers: [{
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptorService,
      multi: true,
    },
    TranslateService,
    ErrorsHandler,
    AuthGuard,
  ],
  entryComponents: [
    ConfirmationDialogComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    private translate: TranslateService,
    private authStore: Store<fromAuth.AuthState>,
  ) {
    this.authStore.pipe(select(fromAuth.getCurrentLang)).pipe(
      take(1),
    ).subscribe((lang) => this.translate.setDefaultLang(lang));
  }
}
