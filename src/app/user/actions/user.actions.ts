import { Action } from '@ngrx/store';

import { User } from '../models/user';
import { AddUserParams } from '../models/add-user-params';
import { Email } from '../models/email';
import { EditUserParams } from '../models/edit-user';
import { GetUserHistoryLoginParams } from '../models/get-user-login-history-params';
import { GetUserLoginHistoryResponse } from '../models/get-user-login-history-response';
import { GetUsersResponse } from '../models/get-users-response';
import { Pagination } from '../../shared/models/pagination';

export enum UserActionTypes {
  GetUsers                   = '[User] Get users',
  GetUsersSuccess            = '[User] Get users success',
  GetUsersFailure            = '[User] Get users failure',
  DeleteUser                 = '[User] Delete user',
  DeleteUserSuccess          = '[User] Delete user success',
  DeleteUserFailure          = '[User] Delete user failure',
  AddUser                    = '[User] Add user',
  AddUserSuccess             = '[User] Add user success',
  AddUserFailure             = '[User] Add user failure',
  GetUserLoginHistory        = '[User] Get user login history',
  GetUserLoginHistorySuccess = '[User] Get user login history success',
  GetUserLoginHistoryFailure = '[User] Get user login history failure',
  EditUser                   = '[User] Edit user',
  EditUserSuccess            = '[User] Edit user success',
  EditUserFailure            = '[User] Edit user failure',
}

export class GetUsers implements Action {
  readonly type = UserActionTypes.GetUsers;

  constructor(public payload: Pagination) { }
}

export class GetUsersSuccess implements Action {
  readonly type = UserActionTypes.GetUsersSuccess;

  constructor(public payload: GetUsersResponse) { }
}

export class GetUsersFailure implements Action {
  readonly type = UserActionTypes.GetUsersFailure;

  constructor(public payload: any) { }
}

export class DeleteUser implements Action {
  readonly type = UserActionTypes.DeleteUser;

  constructor(public payload: Email) { }
}

export class DeleteUserSuccess implements Action {
  readonly type = UserActionTypes.DeleteUserSuccess;

  constructor(public payload: Email) { }
}

export class DeleteUserFailure implements Action {
  readonly type = UserActionTypes.DeleteUserFailure;

  constructor(public payload: any) { }
}

export class AddUser implements Action {
  readonly type = UserActionTypes.AddUser;

  constructor(public payload: AddUserParams) { }
}

export class AddUserSuccess implements Action {
  readonly type = UserActionTypes.AddUserSuccess;

  constructor(public payload: User) { }
}

export class AddUserFailure implements Action {
  readonly type = UserActionTypes.AddUserFailure;

  constructor(public payload: any) { }
}

export class GetUserLoginHistory implements Action {
  readonly type = UserActionTypes.GetUserLoginHistory;

  constructor(public payload?: GetUserHistoryLoginParams) { }
}

export class GetUserLoginHistorySuccess implements Action {
  readonly type = UserActionTypes.GetUserLoginHistorySuccess;

  constructor(public payload: GetUserLoginHistoryResponse) { }
}

export class GetUserLoginHistoryFailure implements Action {
  readonly type = UserActionTypes.GetUserLoginHistoryFailure;

  constructor(public payload: any) { }
}

export class EditUser implements Action {
  readonly type = UserActionTypes.EditUser;

  constructor(public payload: EditUserParams) { }
}

export class EditUserSuccess implements Action {
  readonly type = UserActionTypes.EditUserSuccess;

  constructor(public payload: User) { }
}

export class EditUserFailure implements Action {
  readonly type = UserActionTypes.EditUserFailure;

  constructor(public payload: any) { }
}

export type UserActions =
  | GetUsers
  | GetUsersSuccess
  | GetUsersFailure
  | DeleteUser
  | DeleteUserSuccess
  | DeleteUserFailure
  | AddUser
  | AddUserSuccess
  | AddUserFailure
  | GetUserLoginHistory
  | GetUserLoginHistorySuccess
  | GetUserLoginHistoryFailure
  | EditUser
  | EditUserSuccess
  | EditUserFailure;
