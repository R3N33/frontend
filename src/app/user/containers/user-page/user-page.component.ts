import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { MatDialog, PageEvent } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { Actions, ofType } from '@ngrx/effects';
import { take, map} from 'rxjs/operators';

import * as fromUser from '../../reducers';
import * as fromAuth from '../../../auth/reducers';
import { User } from '../../models/user';
import {
  GetUsers,
  DeleteUser,
  AddUser,
  UserActionTypes,
  GetUserLoginHistory,
  GetUserLoginHistorySuccess,
  EditUser
} from '../../actions/user.actions';
import {
  ConfirmationDialogComponent,
} from '../../../shared/components/confirmation-dialog/confirmation-dialog.component';
import { UserInfo } from '../../../auth/models/user-info';
import { AddUserParams } from '../../models/add-user-params';
import { Email } from '../../models/email';
import { LoginHistoryDialogComponent } from '../../components/login-history-dialog/login-history-dialog.component';
import { EditUserParams } from '../../models/edit-user';
import { GetUserLoginHistoryResponse } from '../../models/get-user-login-history-response';
import { Pagination } from '../../../shared/models/pagination';
import { GetUserHistoryLoginParams } from '../../models/get-user-login-history-params';
import { INITIAL_PAGE_SIZE, LOGIN_HISTORY_DIALOG_ID, PAGE_SIZE_OPTIONS } from '../../../app.constants';
import { GetUsersResponse } from '../../models/get-users-response';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {
  @ViewChild('formDirective', { static: true }) private formDirective: NgForm;

  userForm: FormGroup;
  userInfo$: Observable<UserInfo>;
  isPending$: Observable<boolean>;
  editingEmail: string;
  getUsersResponse$: Observable<GetUsersResponse>;
  loginHistoryResponse$: Observable<GetUserLoginHistoryResponse>;

  PAGE_SIZE_OPTIONS = PAGE_SIZE_OPTIONS;
  pageSize          = INITIAL_PAGE_SIZE;
  displayedColumns  = ['id', 'name', 'email', 'isActivated', 'actions'];
  emptyUser         = {
    id: null,
    name: null,
    email: null,
    isActivated: null,
  } as User;

  constructor(
    private cdRef: ChangeDetectorRef,
    private actions: Actions,
    private matDialog: MatDialog,
    private userStore: Store<fromUser.UserState>,
    private authStore: Store<fromAuth.AuthState>,
    private formBuilder: FormBuilder,
    private translateService: TranslateService,
  ) {
    this.userInfo$             = this.authStore.pipe(select(fromAuth.getUserInfo));
    this.isPending$            = this.userStore.pipe(select(fromUser.getIsPending));
    this.getUsersResponse$     = this.userStore.pipe(select(fromUser.getUsersResponse));
    this.loginHistoryResponse$ = this.userStore.pipe(select(fromUser.getLoginHistoryResponse));
    this.userForm              = this.formBuilder.group({
      name: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
    }, { updateOn: 'submit' });
  }

  ngOnInit(): void {
    this.userStore.dispatch(new GetUsers({ offset: 0, limit: INITIAL_PAGE_SIZE } as Pagination));
  }

  onLoginHistoryClick(email: string): void {
    const pagination                = { offset: 0, limit: INITIAL_PAGE_SIZE } as Pagination;
    const getUserHistoryLoginParams = { email, pagination } as GetUserHistoryLoginParams;

    this.userStore.dispatch(new GetUserLoginHistory(getUserHistoryLoginParams));

    this.actions.pipe(
      ofType(UserActionTypes.GetUserLoginHistorySuccess),
      map((action: GetUserLoginHistorySuccess) => action.payload),
      take(1),
    ).subscribe((loginHistoryResponse) => {
      const dialogRef = this.matDialog.open(LoginHistoryDialogComponent, {
        width: '1000px',
        data: { loginHistoryResponse, isPending: false },
        id: LOGIN_HISTORY_DIALOG_ID,
      });

      const getNewHistory$ = dialogRef.componentInstance.pagination.subscribe((p: Pagination) => {
        this.userStore.dispatch(new GetUserLoginHistory({ email, pagination: p } as GetUserHistoryLoginParams));
        this.cdRef.detectChanges();
      });

      const history$ = this.loginHistoryResponse$.subscribe((response) => {
        dialogRef.componentInstance.data.loginHistoryResponse = response;
      });

      const pending$ = this.isPending$.subscribe((isPending) => {
        dialogRef.componentInstance.data.isPending = isPending;
      });

      dialogRef.afterClosed().subscribe(() => {
        getNewHistory$.unsubscribe();
        history$.unsubscribe();
        pending$.unsubscribe();
      });
    });
  }

  onDeleteUserClick(email: string): void {
    this.translateService.get('userPage.deleteUserConfirmation').subscribe((translation) => {
      const matDialogRef = this.matDialog.open(ConfirmationDialogComponent, {
        width: '400px',
        data: `${translation} ${email}?`,
      });

      matDialogRef.afterClosed().subscribe((result) => {
        if (result) {
          this.userStore.dispatch(new DeleteUser({ email } as Email));
        }
      });
    });
  }

  addEmptyUser(getUsersResponse: GetUsersResponse): User[] {
    return [...getUsersResponse.users, this.emptyUser];
  }

  onSubmit(): void {
    if (this.editingEmail === undefined && this.userForm.invalid) {
      return;
    } else if (this.editingEmail === undefined) {
      this.userStore.dispatch(new AddUser(this.userForm.value as AddUserParams));

      this.actions.pipe(
        ofType(UserActionTypes.AddUserSuccess),
        take(1),
      ).subscribe(() => this.resetForm());
    }

    if (this.editingEmail !== undefined && this.userForm.controls.name.errors) {
      this.resetForm();

      return;
    } else if (this.editingEmail !== undefined) {
      const editUserParams = { name: this.userForm.value.name, email: this.editingEmail } as EditUserParams;

      this.userForm.controls.email.setErrors(null);
      this.userStore.dispatch(new EditUser(editUserParams));

      this.actions.pipe(
        ofType(UserActionTypes.EditUserSuccess),
        take(1),
      ).subscribe(() => {
        this.editingEmail = undefined;

        this.resetForm();
      });
    }
  }

  onPageEvent(pageEvent: PageEvent): void {
    const pagination = { offset: pageEvent.pageIndex * pageEvent.pageSize, limit: pageEvent.pageSize } as Pagination;

    this.userStore.dispatch(new GetUsers(pagination));
  }

  isLoginHistoryDialogOpen(): boolean {
    return !!this.matDialog.getDialogById(LOGIN_HISTORY_DIALOG_ID);
  }

  private resetForm(): void {
    this.formDirective.resetForm();
    this.userForm.reset();
  }
}
