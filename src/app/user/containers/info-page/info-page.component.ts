import { Component, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { take, filter } from 'rxjs/operators';

import * as fromUser from '../../reducers';
import * as fromAuth from '../../../auth/reducers';
import { GetUserLoginHistory } from '../../actions/user.actions';
import { UserInfo } from '../../../auth/models/user-info';
import { Pagination } from '../../../shared/models/pagination';
import { GetUserHistoryLoginParams } from '../../models/get-user-login-history-params';
import { GetUserLoginHistoryResponse } from '../../models/get-user-login-history-response';

@Component({
  selector: 'app-info-page',
  templateUrl: './info-page.component.html',
  styleUrls: ['./info-page.component.scss']
})
export class InfoPageComponent {
  userInfo$: Observable<UserInfo>;
  isPending$: Observable<boolean>;
  loginHistoryResponse$: Observable<GetUserLoginHistoryResponse>;

  constructor(
    private cdRef: ChangeDetectorRef,
    private userStore: Store<fromUser.UserState>,
    private authStore: Store<fromAuth.AuthState>,
  ) {
    this.userInfo$             = this.authStore.pipe(select(fromAuth.getUserInfo));
    this.isPending$            = this.userStore.pipe(select(fromUser.getIsPending));
    this.loginHistoryResponse$ = this.userStore.pipe(select(fromUser.getLoginHistoryResponse));
  }

  onPaginationChange(pagination: Pagination): void {
    this.userInfo$.pipe(
      filter((userInfo) => !!userInfo),
      take(1),
    ).subscribe((userInfo) => {
      const getUserHistoryLoginParams = { email: userInfo.email, pagination } as GetUserHistoryLoginParams;

      this.userStore.dispatch(new GetUserLoginHistory(getUserHistoryLoginParams));
      this.cdRef.detectChanges();
    });
  }
}
