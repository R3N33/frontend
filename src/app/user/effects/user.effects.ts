import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, catchError, exhaustMap } from 'rxjs/operators';
import { of } from 'rxjs';

import { UserService } from '../services/user.service';
import {
  UserActionTypes,
  GetUsersSuccess,
  GetUsersFailure,
  DeleteUserSuccess,
  DeleteUserFailure,
  DeleteUser,
  AddUser,
  AddUserSuccess,
  AddUserFailure,
  GetUserLoginHistory,
  GetUserLoginHistorySuccess,
  GetUserLoginHistoryFailure,
  EditUser,
  EditUserSuccess,
  EditUserFailure,
  GetUsers
} from '../actions/user.actions';
import { User } from '../models/user';
import { AddUserParams } from '../models/add-user-params';
import { Email } from '../models/email';
import { EditUserParams } from '../models/edit-user';
import { GetUserHistoryLoginParams } from '../models/get-user-login-history-params';
import { GetUserLoginHistoryResponse } from '../models/get-user-login-history-response';
import { GetUsersResponse } from '../models/get-users-response';

@Injectable()
export class UserEffects {
  constructor(
    private actions$: Actions,
    private userService: UserService,
  ) { }

  @Effect()
  getUsers$ = this.actions$.pipe(
    ofType(UserActionTypes.GetUsers),
    map((action: GetUsers) => action.payload),
    exhaustMap((pagination) =>
      this.userService.getUsers$(pagination).pipe(
        map((response: GetUsersResponse) => new GetUsersSuccess(response)),
        catchError((error: any) => of(new GetUsersFailure(error))),
      ),
    ),
  );

  @Effect()
  deleteUser$ = this.actions$.pipe(
    ofType(UserActionTypes.DeleteUser),
    map((action: DeleteUser) => action.payload),
    exhaustMap((deleteUserParams) =>
      this.userService.deleteUser$(deleteUserParams).pipe(
        map((response: Email) => new DeleteUserSuccess(response)),
        catchError((error: any) => of(new DeleteUserFailure(error))),
      ),
    ),
  );

  @Effect()
  addUser$ = this.actions$.pipe(
    ofType(UserActionTypes.AddUser),
    map((action: AddUser) => action.payload),
    exhaustMap((addUserParams: AddUserParams) =>
      this.userService.addUser$(addUserParams).pipe(
        map((response: User) => new AddUserSuccess(response)),
        catchError((error: any) => of(new AddUserFailure(error))),
      ),
    ),
  );

  @Effect()
  editUser$ = this.actions$.pipe(
    ofType(UserActionTypes.EditUser),
    map((action: EditUser) => action.payload),
    exhaustMap((editUserParams: EditUserParams) =>
      this.userService.editUser$(editUserParams).pipe(
        map((response: User) => new EditUserSuccess(response)),
        catchError((error: any) => of(new EditUserFailure(error))),
      ),
    ),
  );

  @Effect()
  getUserLoginHistory$ = this.actions$.pipe(
    ofType(UserActionTypes.GetUserLoginHistory),
    map((action: GetUserLoginHistory) => action.payload),
    exhaustMap((getUserHistoryLoginParams: GetUserHistoryLoginParams) =>
      this.userService.getUserLoginHistory$(getUserHistoryLoginParams).pipe(
        map((response: GetUserLoginHistoryResponse) => new GetUserLoginHistorySuccess(response)),
        catchError((error: any) => of(new GetUserLoginHistoryFailure(error))),
      ),
    ),
  );
}
