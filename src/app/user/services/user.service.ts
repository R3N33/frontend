import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { User } from '../models/user';
import { API_ENDPOINTS } from '../../app.constants';
import { AddUserParams } from '../models/add-user-params';
import { Email } from '../models/email';
import { EditUserParams } from '../models/edit-user';
import { GetUserHistoryLoginParams } from '../models/get-user-login-history-params';
import { GetUserLoginHistoryResponse } from '../models/get-user-login-history-response';
import { Pagination } from '../../shared/models/pagination';
import { GetUsersResponse } from '../models/get-users-response';

@Injectable()
export class UserService {
  private userUrl             = `API_URL/${API_ENDPOINTS.users}`;
  private addUserUrl          = `API_URL/${API_ENDPOINTS.addNewUser}`;
  private editUserUrl         = `API_URL/${API_ENDPOINTS.editUser}`;
  private deleteUserUrl       = `API_URL/${API_ENDPOINTS.deleteUser}`;
  private userLoginHistoryUrl = `API_URL/${API_ENDPOINTS.userLoginHistory}`;

  constructor(private httpClient: HttpClient) { }

  getUsers$(pagination: Pagination): Observable<GetUsersResponse> {
    return this.httpClient.post<GetUsersResponse>(this.userUrl, pagination);
  }

  deleteUser$(deleteUserParams: Email): Observable<Email> {
    return this.httpClient.post<Email>(this.deleteUserUrl, deleteUserParams);
  }

  addUser$(addUserParams: AddUserParams): Observable<User> {
    return this.httpClient.post<User>(this.addUserUrl, addUserParams);
  }

  getUserLoginHistory$(params?: GetUserHistoryLoginParams): Observable<GetUserLoginHistoryResponse> {
    if (params) {
      return this.httpClient.post<GetUserLoginHistoryResponse>(this.userLoginHistoryUrl, params);
    } else {
      return this.httpClient.get<GetUserLoginHistoryResponse>(this.userLoginHistoryUrl);
    }
  }

  editUser$(editUserParams: EditUserParams): Observable<User> {
    return this.httpClient.put<User>(this.editUserUrl, editUserParams);
  }
}
