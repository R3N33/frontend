import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { UserPageComponent } from './containers/user-page/user-page.component';
import { UserRoutingModule } from './user-routing.module';
import { reducers } from './reducers';
import { UserEffects } from './effects/user.effects';
import { SharedModule } from '../shared/shared.module';
import { UserService } from './services/user.service';
import { InfoPageComponent } from './containers/info-page/info-page.component';
import { LoginHistoryDialogComponent } from './components/login-history-dialog/login-history-dialog.component';
import { LoginHistoryTableComponent } from './components/login-history-table/login-history-table.component';

@NgModule({
  declarations: [
    UserPageComponent,
    InfoPageComponent,
    LoginHistoryDialogComponent,
    LoginHistoryTableComponent,
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    StoreModule.forFeature('user', reducers, {}),
    EffectsModule.forFeature([UserEffects]),
    SharedModule,
  ],
  exports: [],
  entryComponents: [
    LoginHistoryDialogComponent,
  ],
  providers: [
    UserService,
  ],
})
export class UserModule { }
