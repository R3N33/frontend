import { Component, Inject, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { Pagination } from '../../../shared/models/pagination';
import { GetUserLoginHistoryResponse } from '../../models/get-user-login-history-response';

@Component({
  selector: 'app-login-history-dialog',
  templateUrl: './login-history-dialog.component.html',
  styleUrls: ['./login-history-dialog.component.scss']
})
export class LoginHistoryDialogComponent {
  pagination = new EventEmitter<Pagination>();

  constructor(
    public dialogRef: MatDialogRef<LoginHistoryDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { loginHistoryResponse: GetUserLoginHistoryResponse, isPending: boolean },
  ) { }

  close(): void {
    this.dialogRef.close();
  }

  onPaginationChange(pagination: Pagination): void {
    this.pagination.emit(pagination);
  }
}
