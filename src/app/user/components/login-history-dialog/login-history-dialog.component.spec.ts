import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginHistoryDialogComponent } from './login-history-dialog.component';

describe('LoginHistoryDialogComponent', () => {
  let component: LoginHistoryDialogComponent;
  let fixture: ComponentFixture<LoginHistoryDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginHistoryDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginHistoryDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
