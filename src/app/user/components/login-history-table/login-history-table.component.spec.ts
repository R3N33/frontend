import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginHistoryTableComponent } from './login-history-table.component';

describe('LoginHistoryTableComponent', () => {
  let component: LoginHistoryTableComponent;
  let fixture: ComponentFixture<LoginHistoryTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginHistoryTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginHistoryTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
