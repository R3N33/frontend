import {
  Component,
  Input,
  ViewChildren,
  QueryList,
  ChangeDetectorRef,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
  AfterViewInit
} from '@angular/core';
import { MatSort, MatTableDataSource, PageEvent } from '@angular/material';

import { LoginHistory } from '../../models/login-history';
import { Pagination } from '../../../shared/models/pagination';
import { PAGE_SIZE_OPTIONS, INITIAL_PAGE_SIZE } from '../../../app.constants';

@Component({
  selector: 'app-login-history-table',
  templateUrl: './login-history-table.component.html',
  styleUrls: ['./login-history-table.component.scss']
})
export class LoginHistoryTableComponent implements OnInit, OnChanges, AfterViewInit {
  @ViewChildren(MatSort) sort: QueryList<MatSort>;

  @Input() isPending: boolean;
  @Input() loginHistory: LoginHistory[];
  @Input() pageItemsLength: number;

  @Output() pagination = new EventEmitter<Pagination>();

  dataSource: MatTableDataSource<LoginHistory>;

  pageSize          = INITIAL_PAGE_SIZE;
  displayedColumns  = ['id', 'email', 'datetime'];
  PAGE_SIZE_OPTIONS = PAGE_SIZE_OPTIONS;

  constructor(private cdRef: ChangeDetectorRef) { }

  ngOnInit(): void {
    if (!this.pageItemsLength) {
      this.pagination.emit({ offset: 0, limit: this.pageSize } as Pagination);
    }
  }

  ngOnChanges(): void {
    if (this.pageItemsLength && this.sort) {
      this.dataSource      = new MatTableDataSource(this.loginHistory);
      this.dataSource.sort = this.sort.first;

      this.cdRef.detectChanges();
    }
  }

  ngAfterViewInit(): void {
    this.dataSource      = new MatTableDataSource(this.loginHistory);
    this.dataSource.sort = this.sort.first;

    this.cdRef.detectChanges();
  }

  onPageEvent(pageEvent: PageEvent): void {
    this.pagination.emit({ offset: pageEvent.pageIndex * pageEvent.pageSize, limit: pageEvent.pageSize } as Pagination);
  }
}
