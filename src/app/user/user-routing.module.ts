import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '../auth/services/auth-guard.service';
import { UserPageComponent } from './containers/user-page/user-page.component';
import { ROUTES } from '../app.constants';
import { InfoPageComponent } from './containers/info-page/info-page.component';

const routes: Routes = [
  {
    path: ROUTES.users,
    component: UserPageComponent,
    canActivate: [AuthGuard],
  },
  {
    path: ROUTES.usersInfo,
    component: InfoPageComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoutingModule { }
