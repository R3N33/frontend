export interface User {
  id: number;
  name: string;
  email: string;
  isActivated: number;
}
