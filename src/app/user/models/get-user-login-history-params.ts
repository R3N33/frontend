import { Email } from './email';
import { Pagination } from 'src/app/shared/models/pagination';

export interface GetUserHistoryLoginParams extends Email {
  pagination: Pagination;
}
