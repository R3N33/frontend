export interface AddUserParams {
  name: string;
  email: string;
}
