export interface LoginHistory {
  id: number;
  email: string;
  datetime: string;
}
