import { LoginHistory } from './login-history';

export interface GetUserLoginHistoryResponse {
  loginHistory: LoginHistory[];
  total: number;
}
