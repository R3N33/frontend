export interface EditUserParams {
  name: string;
  email: string;
}
