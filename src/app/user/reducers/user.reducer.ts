import * as moment from 'moment';

import { UserActions, UserActionTypes } from '../actions/user.actions';
import { GetUserLoginHistoryResponse } from '../models/get-user-login-history-response';
import { GetUsersResponse } from '../models/get-users-response';

export interface UserState {
  getUsersResponse: GetUsersResponse;
  loginHistoryResponse: GetUserLoginHistoryResponse;
  isPending: boolean;
  error: any;
}

export const initialState: UserState = {
  getUsersResponse: null,
  loginHistoryResponse: null,
  isPending: false,
  error: null,
};

export function reducer(state = initialState, action: UserActions): UserState {
  switch (action.type) {
    case UserActionTypes.GetUsers: {
      return {
        ...state,
        isPending: true,
      };
    }

    case UserActionTypes.GetUsersSuccess: {
      return {
        ...state,
        getUsersResponse: {
          users: action.payload.users,
          total: action.payload.total,
        },
        isPending: false,
      };
    }

    case UserActionTypes.GetUsersFailure: {
      return {
        ...state,
        isPending: false,
        error: action.payload,
      };
    }

    case UserActionTypes.DeleteUser: {
      return {
        ...state,
        isPending: true,
      };
    }

    case UserActionTypes.DeleteUserSuccess: {
      return {
        ...state,
        getUsersResponse: {
          users: [...state.getUsersResponse.users].filter((user) => user.email !== action.payload.email),
          total: state.getUsersResponse.total - 1,
        },
        isPending: false,
      };
    }

    case UserActionTypes.DeleteUserFailure: {
      return {
        ...state,
        isPending: false,
        error: action.payload,
      };
    }

    case UserActionTypes.AddUser: {
      return {
        ...state,
        isPending: true,
      };
    }

    case UserActionTypes.AddUserSuccess: {
      return {
        ...state,
        getUsersResponse: {
          users: [...state.getUsersResponse.users, action.payload],
          total: state.getUsersResponse.total + 1,
        },
        isPending: false,
      };
    }

    case UserActionTypes.AddUserFailure: {
      return {
        ...state,
        isPending: false,
        error: action.payload,
      };
    }

    case UserActionTypes.GetUserLoginHistory: {
      return {
        ...state,
        isPending: true,
      };
    }

    case UserActionTypes.GetUserLoginHistorySuccess: {
      const loginHistory = [...action.payload.loginHistory];

      loginHistory.forEach((historyItem) => {
        const utc            = moment.utc(historyItem.datetime);
        historyItem.datetime = moment(utc).local().format('YYYY-MM-DD HH:mm:ss');
      });

      return {
        ...state,
        loginHistoryResponse: {
          loginHistory,
          total: action.payload.total,
        },
        isPending: false,
      };
    }

    case UserActionTypes.GetUserLoginHistoryFailure: {
      return {
        ...state,
        isPending: false,
        error: action.payload,
      };
    }

    case UserActionTypes.EditUser: {
      return {
        ...state,
        isPending: true,
      };
    }

    case UserActionTypes.EditUserSuccess: {
      const users = [...state.getUsersResponse.users];

      users.forEach((user) => {
        if (user.id === action.payload.id) {
          user.name = action.payload.name;
        }
      });

      return {
        ...state,
        getUsersResponse: {
          users,
          total: state.getUsersResponse.total,
        },
        isPending: false,
      };
    }

    case UserActionTypes.EditUserFailure: {
      return {
        ...state,
        isPending: false,
        error: action.payload,
      };
    }

    default: {
      return state;
    }
  }
}

export const getError                = (state: UserState) => state.error;
export const getIsPending            = (state: UserState) => state.isPending;
export const getUsersResponse        = (state: UserState) => state.getUsersResponse;
export const getLoginHistoryResponse = (state: UserState) => state.loginHistoryResponse;
