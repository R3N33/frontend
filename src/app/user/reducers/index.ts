
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromUser from './user.reducer';

export interface UserState {
  user: fromUser.UserState;
}

export const reducers: ActionReducerMap<UserState> = {
  user: fromUser.reducer,
};

export const selectUserState = createSelector(
  createFeatureSelector<UserState>('user'),
  (state: UserState) => state.user,
);

export const getUsersResponse = createSelector(
  selectUserState,
  fromUser.getUsersResponse,
);

export const getLoginHistoryResponse = createSelector(
  selectUserState,
  fromUser.getLoginHistoryResponse,
);

export const getError = createSelector(
  selectUserState,
  fromUser.getError,
);

export const getIsPending = createSelector(
  selectUserState,
  fromUser.getIsPending,
);
